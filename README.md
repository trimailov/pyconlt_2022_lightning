# Asyncio - generators with syntactic sugar?

Justas Trimailovas

2022-05-27, Lightning talk, PyConLT

[gitlab.com/trimailov](https://gitlab.com/trimailov/pyconlt_2022_lightning)

More details (with minimal code examples and data sources):
- [Part 1](https://gitlab.com/trimailov/be_guild_async_python/)
- [Part 2](https://gitlab.com/trimailov/be_guild_async_python2/)

This was mentioned for 10s by James Powell in talk during first day -
["`int` is to `list` as `float` is to `tuple`"](https://pretalx.com/pycon-lt-2022/talk/UFKCNL/)

## Motivation

People are telling "just add async/await and don't worry about it",
but I don't understand it, because async/await is unusual and quirky to use.

Research to better understand `asyncio`.

To share the "obvious"?

## History

Python went from idea of a "generator" to implementing cooperative multitasking
with async/await... and it took us only 20+ years.

- ~1995-2000s - Guido contemplates cooperative multitasking (but lost the source :( );
- Python2.2, PEP 255 ( https://www.python.org/dev/peps/pep-0255/ ):
    - this happened in 2001 - 20 years ago!
    - `yield` statement introduced;
    - `yield` - freezes the state of the function and "yields" execution to the caller;
    - as a python user we know it as a way to "generate" data and/or save memory;
- Python2.5, PEP 342 ( https://www.python.org/dev/peps/pep-0342/ ):
    - `yield` becomes an expression and not a statement, i.e. `result = yield` is possible;
    - `send()` function to send values to generator;
    - `throw()` function to send exceptions to generator;
    - `close()` function to send `GeneratorExit` to generator;
- Python3.3, PEP 380 ( https://www.python.org/dev/peps/pep-0380/ ):
    - `yield from` - allows to yield values from other iterators (subgenerator delegation);
- Python3.4, PEP 3156 ( https://www.python.org/dev/peps/pep-3156/ ):
    - `asyncio` library (event loop, protocols, transports, scheduler, etc.):
- Python3.5, PEP 492 ( https://www.python.org/dev/peps/pep-0492/ ):
    - `async/await`;
    - `async def` = coroutine function;
    - `await` = basically a `yield from` with strictness to awaitable objects
      (coroutines, `types.coroutine()`, `__await__()`);

## Generator coroutines vs async/await

```
Generators                        | asyncio
==================================+===================================
    calling like a function returns an object, but not executes it
----------------------------------+-----------------------------------
.__next__()                       | .__await__()
----------------------------------+-----------------------------------
yield instead of return           | async def
----------------------------------+-----------------------------------
yield from                        | await
----------------------------------+-----------------------------------
simple call returns generator obj | simple call returns coroutine obj
----------------------------------+-----------------------------------
no event loop                     | event loop and friends included
----------------------------------+-----------------------------------
```

## Summary

Generators - allows programmer to control when function is stopped.

Generators coroutines - can accept values.

Sprinkle some syntactic sugar with event loop - and you have async/await.
